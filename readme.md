# Prueba Front

Dado el siguiente diseño:


## 1º Maquetar usando HTML5 y SCSS como preprocesador de CSS. El diseño ha de ser aproximado no se va a valorar si el margen mide 8px en lugar de 5px si no que exista el margen

## 2º La Validación del formulario será obligatorio que ambos inputs estén cumplimentados, uno será tipo email con validación email y otro password cuya validación será que al menos deberá de tener 5 caracteres. El reminder del formulario, será un switch de tipo booleano y opcional.

## 3º El botón de aceptar siempre estará activo pero al pulsarlo la primera vez si el formulario está mal cumplimentado se mostraran los mensajes de error, en caso de estar correctamente cumplimentados con poner un console.log(‘OK’) será suficiente

## 4º Para los iconos usar la librería que viene en Ionic o cualesquiera, el objetivo no es el diseño del icono si no su correcta maquetación.

## 5º Realizar el diseño responsive  con el breakpoint en 960px.

## 6º Se valorará positivamente la creatividad (por ejemplo añadir alguna animación)

## 7º Test unitarios, E2E y componentes

Se valorará el uso de test unitarios, E2E y componentes

## 8º Inicialización del proyecto

Bastará con inicializar un proyecto en blanco mediante el CLI de Ionic o el CLI de Angular dependiendo del puesto si es web o mobile.

Mobile
ionic start test_app blank

Web
ng new test_app

## 9º Dudas

El proyecto se debe subir a algún repositorio publico (github, gitlab) para poder revisar la prueba
Cualquier duda enviar mail a arquitecturamobile@sanitas.es / arquitecturafront@sanitas.es.